# Crown Peak Interview Velimir



## Getting started

1. clone the repository on your local machine
2. Run 'npm install' in the terminal
3. Run 'yarn start' to start the development server
4. Open 'localhost:3000' in the browser to see the application

## Source code

All the application code is found within the '/src/app' folder, all other folders contain utility and configuration files that don't need to be modified.
