export type LogItemType =
  | {
      timestamp: number;
      url: string;
      response_time: number;
      status: 1 | 2;
      issue_type: number;
      issue_description: string;
      date?: string;
    }
  | {
      timestamp: number;
      url: string;
      response_time: number;
      status: 0;
      issue_type: never;
      issue_description: never;
      date?: string;
    };
