export type BarLogItemsType = {
  date: string;
  successfullRequests: number;
  warnings: number;
  errors: number;
};
