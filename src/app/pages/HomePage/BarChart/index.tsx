/**
 *
 * BarChart
 *
 */
import * as React from 'react';
import * as d3 from 'd3';
import { useEffect, useRef } from 'react';
import styled from 'styled-components';

interface Props {
  data: any;
}

export function BarChart(props: Props) {
  const axisBottomRef = useRef<SVGGElement>(null);
  const axisLeftRef = useRef<SVGGElement>(null);
  const { data } = props;

  const margin = { top: 10, right: 0, bottom: 20, left: 30 };
  const width = data.length * 100 - margin.left - margin.right;
  const height = 300 - margin.top - margin.bottom;

  const subgroups = ['successfullRequests', 'warnings', 'errors'];

  const labels = data.map(entry => entry.date);
  const max = Math.max(
    ...data.map((entry: any) => {
      return entry.successfullRequests + entry.warnings + entry.errors;
    }),
  );

  console.log('labels', labels);
  let totalGroupCount = Array<number>(); // eslint-disable-line
  for (let entry of data) {
    totalGroupCount.push(entry.requests);
  }
  const scaleX = d3.scaleBand().domain(labels).range([0, width]).padding(0.3);
  const scaleY = d3.scaleLinear().domain([0, max]).range([height, 0]);
  const color = d3
    .scaleOrdinal<string>()
    .domain(subgroups)
    .range(['#1ae457', '#e2c534f1', '#eb1c1c']);

  const stacked = d3.stack().keys(subgroups)(data);
  console.log('max', max, stacked);
  useEffect(() => {
    if (axisBottomRef.current) {
      d3.select(axisBottomRef.current).call(d3.axisBottom(scaleX));
    }

    if (axisLeftRef.current) {
      d3.select(axisLeftRef.current).call(d3.axisLeft(scaleY));
    }
  }, [scaleX, scaleY]);
  return (
    <BarChartWrapper>
      <svg
        width={width + margin.left + margin.right}
        height={height + margin.top + margin.bottom}
      >
        <g transform={`translate(${margin.left}, ${margin.top})`}>
          <g ref={axisBottomRef} transform={`translate(0, ${height})`} />
          <g ref={axisLeftRef} />
          {stacked.map((data, index) => {
            return (
              <g key={`group-${index}`} fill={color(data.key)}>
                {data.map((d, index) => {
                  const label = String(d.data.date);
                  const y0 = scaleY(d[0]);
                  const y1 = scaleY(d[1]);

                  return (
                    <rect
                      key={`rect-${index}`}
                      x={scaleX(label)}
                      y={y1}
                      width={scaleX.bandwidth()}
                      height={y0 - y1 || 0}
                    />
                  );
                })}
              </g>
            );
          })}
        </g>
      </svg>
    </BarChartWrapper>
  );
}
const BarChartWrapper = styled.div`
  width: 100vw;
  overflow-x: scroll;
  padding-bottom: 20px;
`;
