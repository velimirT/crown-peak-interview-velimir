import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { homePageSaga } from './saga';
import {
  HomePageState,
  LogItemsErrorType,
  LogItemsIssueType,
  LogItemsStatusType,
} from './types';
import { LogItemType } from 'types/LogItem';

export const initialState: HomePageState = {
  issueType: LogItemsIssueType.ANY,
  status: LogItemsStatusType.ANY,
  minTimestamp: -1,
  maxTimestamp: -1,
  minResponseTime: -1,
  maxResponseTime: -1,
  URI: '',
  logItems: [],
  loading: false,
  error: null,
  chartPeriod: 'day',
};

const slice = createSlice({
  name: 'homePage',
  initialState,
  reducers: {
    filterByIssueType(state, action: PayloadAction<any>) {
      state.issueType = action.payload;
    },
    filterByStatus(state, action: PayloadAction<any>) {
      state.status = action.payload;
    },
    filterByMinTimestamp(state, action: PayloadAction<any>) {
      state.minTimestamp = action.payload;
    },
    filterByMaxTimestamp(state, action: PayloadAction<any>) {
      state.maxTimestamp = action.payload;
    },
    filterByMinResponseTime(state, action: PayloadAction<any>) {
      state.minResponseTime = action.payload;
    },
    filterByMaxResponseTime(state, action: PayloadAction<any>) {
      state.maxResponseTime = action.payload;
    },
    filterByURI(state, action: PayloadAction<any>) {
      state.URI = action.payload;
    },
    setPeriod(state, action: PayloadAction<any>) {
      state.chartPeriod = action.payload;
    },
    loadLogItems(state) {
      state.loading = true;
      state.error = null;
      state.logItems = [];
    },
    logItemsLoaded(state, action: PayloadAction<LogItemType[]>) {
      const logItems = action.payload;
      state.logItems = logItems;
      state.loading = false;
    },
    logItemsError(state, action: PayloadAction<LogItemsErrorType>) {
      state.error = action.payload;
      state.loading = false;
    },
  },
});

export const { actions: homePageActions } = slice;

export const useHomePageSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: homePageSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useHomePageSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
