import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';
import { LogItemsIssueType, LogItemsStatusType } from './types';
import { LogItemType } from 'types/LogItem';
import { BarLogItemsType } from 'types/BarLogItems';
import groupBy from 'object.groupby';

const selectSlice = (state: RootState) => state.homePage || initialState;

export const selectIssueType = createSelector(
  [selectSlice],
  homePageState => homePageState.issueType,
);

export const selectStatus = createSelector(
  [selectSlice],
  homePageState => homePageState.status,
);

export const selectMinTimestamp = createSelector(
  [selectSlice],
  homePageState => homePageState.minTimestamp,
);

export const selectMaxTimestamp = createSelector(
  [selectSlice],
  homePageState => homePageState.maxTimestamp,
);

export const selectMinResponseTime = createSelector(
  [selectSlice],
  homePageState => homePageState.minResponseTime,
);

export const selectMaxResponseTime = createSelector(
  [selectSlice],
  homePageState => homePageState.maxResponseTime,
);

export const selectLoading = createSelector(
  [selectSlice],
  homePageState => homePageState.loading,
);

export const selectError = createSelector(
  [selectSlice],
  homePageState => homePageState.error,
);

export const selectLogItems = createSelector(
  [selectSlice],
  homePageState => homePageState.logItems,
);

export const selectChartPeriod = createSelector(
  [selectSlice],
  homePageState => homePageState.chartPeriod,
);

export const selectFilteredLogItems = createSelector(
  [selectSlice],
  homePageState => {
    return homePageState.logItems.filter(item => {
      let included = true;
      if (homePageState.issueType > LogItemsIssueType.ANY) {
        if (item.issue_type !== homePageState.issueType) {
          included = false;
          return included;
        }
      } //filter by Issue Type
      if (homePageState.status > LogItemsStatusType.ANY) {
        if (item.status !== homePageState.status) {
          included = false;
          return included;
        }
      } //filter by Status
      if (homePageState.minTimestamp > -1) {
        if (item.timestamp < homePageState.minTimestamp) {
          included = false;
          return included;
        }
      } //filter by Min. Timestamp
      if (homePageState.maxTimestamp > -1) {
        if (item.timestamp > homePageState.maxTimestamp) {
          included = false;
          return included;
        }
      } //filter by Max. Timestamp
      if (homePageState.minResponseTime > -1) {
        if (item.response_time < homePageState.minResponseTime) {
          included = false;
          return included;
        }
      } //filter by Min. Timestamp
      if (homePageState.maxResponseTime > -1) {
        if (item.response_time > homePageState.maxResponseTime) {
          included = false;
          return included;
        }
      } //filter by Max. Timestamp
      if (homePageState.URI !== '') {
        const regEx = /(http\/\/||https\/\/)(.*)(?=\/)/g;
        let matches = regEx.exec(item.url)![0];

        const url = homePageState.URI.endsWith('/')
          ? homePageState.URI.substr(0, homePageState.URI.length - 1)
          : homePageState.URI;
        if (matches !== url) {
          included = false;
          return included;
        }
      } //filter by URL
      return included;
    });
  },
);

export const selectBarLogItems = createSelector(
  [selectSlice, selectFilteredLogItems, selectChartPeriod],
  (homePageState, filteredLogItems, chartPeriod) => {
    const barLogItems = filteredLogItems.map((item: LogItemType) => {
      const date = new Date(item.timestamp * 1000);
      let grouping;
      if (chartPeriod === 'day') {
        grouping = `${date.getFullYear()}-${
          date.getMonth() + 1
        }-${date.getDate()}`;
      } else if (chartPeriod === 'hour') {
        grouping = `${date.getFullYear()}-${
          date.getMonth() + 1
        }-${date.getDate()}-${date.getHours()}:00`;
      }
      return {
        date: grouping,
        ...item,
      };
    });

    const groupedItems = groupBy(barLogItems, (item: LogItemType) => {
      return item.date;
    });
    let barSections = new Array<BarLogItemsType>(); // eslint-disable-line
    for (let key in groupedItems) {
      const successfullRequests = groupedItems[key].filter(
        item => item.status === LogItemsStatusType.SUCCESS,
      );
      const warnings = groupedItems[key].filter(
        item => item.status === LogItemsStatusType.WARNING,
      );
      const errors = groupedItems[key].filter(
        item => item.status === LogItemsStatusType.ERROR,
      );
      barSections.push({
        date: groupedItems[key][0].date,
        successfullRequests: successfullRequests.length,
        warnings: warnings.length,
        errors: errors.length,
      });
    }

    console.log('barSections', groupedItems, barSections);
    return barSections;
  },
);
