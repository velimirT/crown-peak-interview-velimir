import { call, put, takeLatest } from 'redux-saga/effects';
import { request } from 'utils/request';
import { homePageActions as actions } from '.';
import { LogItemType } from 'types/LogItem';
import { LogItemsErrorType } from './types';
// import jsonData from '../../../'

export function* getLogs() {
  const requestURL = 'http://localhost:3000/access_logs.json';

  try {
    // Call our request helper (see 'utils/request')
    const logItems: LogItemType[] = yield call(request, requestURL);
    if (logItems?.length > 0) {
      yield put(actions.logItemsLoaded(logItems));
    } else {
      yield put(actions.logItemsError(LogItemsErrorType.USER_HAS_NO_REPO));
    }
  } catch (err: any) {
    if (err.response?.status === 404) {
      yield put(actions.logItemsError(LogItemsErrorType.USER_NOT_FOUND));
    } else if (err.message === 'Failed to fetch') {
      yield put(actions.logItemsError(LogItemsErrorType.GITHUB_RATE_LIMIT));
    } else {
      yield put(actions.logItemsError(LogItemsErrorType.RESPONSE_ERROR));
    }
  }
}

export function* homePageSaga() {
  // Watches for loadRepos actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(actions.loadLogItems.type, getLogs);
}
