import { LogItemType } from 'types/LogItem';
/* --- STATE --- */

export interface HomePageState {
  issueType: number;
  status: number;
  minTimestamp: number;
  maxTimestamp: number;
  minResponseTime: number;
  maxResponseTime: number;
  chartPeriod: 'day' | 'hour';
  URI: string;
  loading: boolean;
  error?: LogItemsErrorType | null;
  logItems: LogItemType[];
}

export const enum LogItemsErrorType {
  RESPONSE_ERROR = 1,
  USER_NOT_FOUND = 2,
  USERNAME_EMPTY = 3,
  USER_HAS_NO_REPO = 4,
  GITHUB_RATE_LIMIT = 5,
}

export const enum LogItemsIssueType {
  ANY = -1,
  MISSING_PARAM = 0,
  RATE_LIMIT = 1,
  NOT_FOUND = 2,
  UNKNOWN_PARAM = 3,
  DEPRECATED = 4,
  UNSECURED = 5,
}

export const enum LogItemsStatusType {
  ANY = -1,
  SUCCESS = 0,
  WARNING = 1,
  ERROR = 2,
}
