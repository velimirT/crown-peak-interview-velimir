/**
 *
 * LogsList
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { LogItemType } from 'types/LogItem';
interface Props {
  logItems: LogItemType[];
  statusText: Function;
  issueTypeText: Function;
}

export function LogsList(props: Props) {
  const { logItems, statusText, issueTypeText } = props;
  return (
    <ul>
      {logItems.map(item => {
        return (
          <li>
            {issueTypeText(item.issue_type)}, {statusText(item.status)},{' '}
            {item.timestamp}
            {item.url.match(/(http\/\/||https\/\/)(.*)(?:\/)/g)}
          </li>
        );
      })}
    </ul>
  );
}

const Div = styled.div``;
