import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components';
import {
  selectIssueType,
  selectStatus,
  selectMinTimestamp,
  selectMaxTimestamp,
  selectMinResponseTime,
  selectMaxResponseTime,
  selectFilteredLogItems,
  selectBarLogItems,
  selectChartPeriod,
} from './slice/selectors';
import { LogItemsIssueType, LogItemsStatusType } from './slice/types';
import { BarLogItemsType } from 'types/BarLogItems';
import { useHomePageSlice } from './slice';
import { BarChart } from './BarChart';
import { LogsList } from './LogsList';

export function HomePage() {
  const { actions } = useHomePageSlice();

  const issueType = useSelector(selectIssueType);
  const status = useSelector(selectStatus);
  const minTimestamp = useSelector(selectMinTimestamp);
  const maxTimestamp = useSelector(selectMaxTimestamp);
  const minResponseTime = useSelector(selectMinResponseTime);
  const maxResponseTime = useSelector(selectMaxResponseTime);
  const logItems = useSelector(selectFilteredLogItems);
  const chartPeriod = useSelector(selectChartPeriod);
  const barLogItems: BarLogItemsType[] = useSelector(selectBarLogItems);

  const dispatch = useDispatch();

  const onChangeIssueType = (evt: React.ChangeEvent<HTMLSelectElement>) => {
    dispatch(actions.filterByIssueType(parseInt(evt.currentTarget.value)));
  };

  const onChangeStatus = (evt: React.ChangeEvent<HTMLSelectElement>) => {
    dispatch(actions.filterByStatus(parseInt(evt.currentTarget.value)));
  };

  const onChangeMinTimestamp = (evt: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(
      actions.filterByMinTimestamp(Date.parse(evt.currentTarget.value) / 1000),
    );
  };

  const onChangeMaxTimestamp = (evt: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(
      actions.filterByMaxTimestamp(Date.parse(evt.currentTarget.value) / 1000),
    );
  };

  const onChangeMinResponseTime = (
    evt: React.ChangeEvent<HTMLInputElement>,
  ) => {
    dispatch(actions.filterByMinResponseTime(evt.currentTarget.value));
  };

  const onChangeMaxResponseTime = (
    evt: React.ChangeEvent<HTMLInputElement>,
  ) => {
    dispatch(actions.filterByMaxResponseTime(evt.currentTarget.value));
  };
  const onChangeURL = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const regEx = /(http\/\/||https\/\/)(.*)(?=\/)/g;
    const matches = regEx.exec(evt.currentTarget.value);
    if (matches !== null) {
      dispatch(actions.filterByURI(evt.currentTarget.value));
    }
  };

  const setPeriod = (period: String) => {
    dispatch(actions.setPeriod(period));
  };

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    dispatch(actions.loadLogItems());
  });
  return (
    <>
      <Helmet>
        <title>HomePage</title>
        <meta name="description" content="A Boilerplate application homepage" />
      </Helmet>
      <span>My HomePage!</span>
      <p>
        <label htmlFor="minTimestamp">Min. Timestamp</label>
        <input
          type="datetime-local"
          defaultValue={minTimestamp}
          onChange={onChangeMinTimestamp}
          id="minTimestamp"
        />
        <label htmlFor="maxTimestamp">Min. Timestamp</label>
        <input
          type="datetime-local"
          defaultValue={maxTimestamp}
          onChange={onChangeMaxTimestamp}
          id="maxTimestamp"
        />
      </p>
      <p>
        <label htmlFor="minResponseTime">Min Response Time</label>
        <input
          type="number"
          id="minResponseTime"
          defaultValue={minResponseTime === -1 ? '' : minResponseTime}
          max={maxResponseTime}
          min={-1}
          onChange={onChangeMinResponseTime}
        />
        <label htmlFor="maxResponseTime">Max Response Time</label>
        <input
          type="number"
          id="maxResponseTime"
          defaultValue={maxResponseTime === -1 ? '' : maxResponseTime}
          min={minResponseTime > -1 ? minResponseTime : -1}
          onChange={onChangeMaxResponseTime}
        />
        <label htmlFor="issueTypeFilter">Issue Type:</label>
        <select
          onChange={onChangeIssueType}
          defaultValue={issueType}
          id="issueTypeFilter"
        >
          <option value={LogItemsIssueType.ANY}>Any</option>
          <option value={LogItemsIssueType.MISSING_PARAM}>
            {issueTypeText(LogItemsIssueType.MISSING_PARAM)}
          </option>
          <option value={LogItemsIssueType.RATE_LIMIT}>
            {issueTypeText(LogItemsIssueType.RATE_LIMIT)}
          </option>
          <option value={LogItemsIssueType.NOT_FOUND}>
            {issueTypeText(LogItemsIssueType.NOT_FOUND)}
          </option>
          <option value={LogItemsIssueType.UNKNOWN_PARAM}>
            {issueTypeText(LogItemsIssueType.UNKNOWN_PARAM)}
          </option>
          <option value={LogItemsIssueType.DEPRECATED}>
            {issueTypeText(LogItemsIssueType.DEPRECATED)}
          </option>
          <option value={LogItemsIssueType.UNSECURED}>
            {issueTypeText(LogItemsIssueType.UNSECURED)}
          </option>
        </select>
        <label htmlFor="statusFilter">Status:</label>
        <select
          onChange={onChangeStatus}
          defaultValue={status}
          id="statusFilter"
        >
          <option value={LogItemsStatusType.ANY}>Any</option>
          <option value={LogItemsStatusType.SUCCESS}>
            {statusText(LogItemsStatusType.SUCCESS)}
          </option>
          <option value={LogItemsStatusType.WARNING}>
            {statusText(LogItemsStatusType.WARNING)}
          </option>
          <option value={LogItemsStatusType.ERROR}>
            {statusText(LogItemsStatusType.ERROR)}
          </option>
        </select>
        <label htmlFor="url">URL</label>
        <input type="text" id="url" onBlur={onChangeURL} />
      </p>
      <div>
        <ChartPeriodButton
          className={chartPeriod === 'hour' ? 'selected' : ''}
          onClick={() => setPeriod('hour')}
        >
          Hour
        </ChartPeriodButton>
        <ChartPeriodButton
          className={chartPeriod === 'day' ? 'selected' : ''}
          onClick={() => setPeriod('day')}
        >
          Day
        </ChartPeriodButton>
      </div>
      <BarChart data={barLogItems} />
      <LogsList
        logItems={logItems}
        issueTypeText={issueTypeText}
        statusText={statusText}
      />
    </>
  );
}

export const issueTypeText = (issueType: LogItemsIssueType) => {
  switch (issueType) {
    case LogItemsIssueType.MISSING_PARAM:
      return 'Missing Parameter';
    case LogItemsIssueType.RATE_LIMIT:
      return 'Rate limit exceeded';
    case LogItemsIssueType.NOT_FOUND:
      return 'Not found';
    case LogItemsIssueType.UNKNOWN_PARAM:
      return 'Unknown parameter';
    case LogItemsIssueType.DEPRECATED:
      return 'Deprecated';
    case LogItemsIssueType.UNSECURED:
      return 'Unsecured';
    default:
      return 'Unknown issue!';
  }
};

export const statusText = (status: LogItemsStatusType) => {
  switch (status) {
    case LogItemsStatusType.SUCCESS:
      return 'Success';
    case LogItemsStatusType.WARNING:
      return 'Warning';
    case LogItemsStatusType.ERROR:
      return 'Error';
    default:
      return 'Unknown status!';
  }
};

const ChartPeriodButton = styled.button`
  background-color: #fff;
  border: none;
  cursor: pointer;
  font-size: 20px;
  &.selected {
    background-color: #2288cb;
    color: #fff;
  }
`;
